within DrivingCycles.DrivingCycles;
record HWFET
  "The Highway Fuel Economy Driving Schedule (HWFET) represents highway driving conditions under 100 km/h"
  extends Base(
    drivingCycleName = "HWFET",
    fileName = Modelica.Utilities.Files.loadResource("modelica://DrivingCycles/Resources/DrivingCycles/HWFET.txt"),
    T = 766);
    annotation(defaultComponentName = "drivingCycle", defaultComponentPrefixes = "parameter",
    Documentation(info="<html>
<p>Data source <a href=\"https://imee.pl/pub/drive-cycles\">https://imee.pl/pub/drive-cycles</a></p>
</html>"));
end HWFET;
