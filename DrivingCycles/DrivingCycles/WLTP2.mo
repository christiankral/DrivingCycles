within DrivingCycles.DrivingCycles;
record WLTP2
  "Worldwide Harmonized Light Vehicle Test Procedure (WLTP) Class 2"
  extends Base(
    drivingCycleName = "WLTP2",
    fileName = Modelica.Utilities.Files.loadResource("modelica://DrivingCycles/Resources/DrivingCycles/WLTP2.txt"),
    T = 1477);
    annotation(defaultComponentName = "drivingCycle", defaultComponentPrefixes = "parameter",
    Documentation(info="<html>
<p>Data source <a href=\"https://imee.pl/pub/drive-cycles\">https://imee.pl/pub/drive-cycles</a></p>
</html>"));
end WLTP2;
