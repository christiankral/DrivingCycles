# DrivingCycles

This is the library provides standard driving cycles obtained from the following public sources:

- https://imee.pl/pub/drive-cycles
- https://www.epa.gov/vehicle-and-fuel-emissions-testing/dynamometer-drive-schedules
