within DrivingCycles.DrivingCycles;
record NYCC
  "The New York City Cycle (NYCC) features low speed stop-and-go traffic conditions"
  extends Base(
    drivingCycleName = "NYCC",
    fileName = Modelica.Utilities.Files.loadResource("modelica://DrivingCycles/Resources/DrivingCycles/NYCC.txt"),
    T = 598);
    annotation(defaultComponentName = "drivingCycle", defaultComponentPrefixes = "parameter",
    Documentation(info="<html>
<p>Data source <a href=\"https://imee.pl/pub/drive-cycles\">https://imee.pl/pub/drive-cycles</a></p>
</html>"));
end NYCC;
