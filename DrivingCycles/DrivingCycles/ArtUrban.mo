within DrivingCycles.DrivingCycles;
record ArtUrban
  "Driving cycle is based on a statistical study done in Europe; driving in urban area"
  extends Base(
    drivingCycleName = "ArtUrban",
    fileName = Modelica.Utilities.Files.loadResource("modelica://DrivingCycles/Resources/DrivingCycles/ArtUrban.txt"),
    T = 994);
    annotation(defaultComponentName = "drivingCycle", defaultComponentPrefixes = "parameter",
    Documentation(info="<html>
<p>Data source <a href=\"https://imee.pl/pub/drive-cycles\">https://imee.pl/pub/drive-cycles</a></p>
</html>"));
end ArtUrban;
