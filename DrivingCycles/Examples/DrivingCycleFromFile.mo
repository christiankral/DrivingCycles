within DrivingCycles.Examples;
model DrivingCycleFromFile "Driving cycle loaded from file"
  extends Modelica.Icons.Example;
  Modelica.Units.SI.Velocity v = combiTimeTable.y[1] "Velocity";
  Modelica.Blocks.Sources.CombiTimeTable combiTimeTable(
    tableOnFile=true,
    tableName="velocity",
    fileName=drivingCycle.fileName,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic)
    annotation (Placement(transformation(extent={{-80,-10},{-60,10}})));
  parameter DrivingCycles.WLTP1 drivingCycle
    annotation (Placement(transformation(extent={{-80,32},{-60,52}})));
  annotation (experiment(
      StopTime=1022,
      __Dymola_NumberOfIntervals=1022,
      Tolerance=1e-06,
      __Dymola_Algorithm="Dassl"), Documentation(info="<html>
<p>The driving cycle can be changed by removing the driving cycle record <code>drivingCycle</code>
and dragging a different driving cycle from the package
<a href=\"modelica://DrivingCycles.DrivingCycles\">DrivingCycles.DrivingCycles</a>.
</html>"));
end DrivingCycleFromFile;
