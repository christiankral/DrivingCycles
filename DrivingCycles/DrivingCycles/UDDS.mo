within DrivingCycles.DrivingCycles;
record UDDS
  "The EPA Urban Dynamometer Driving Schedule (UDDS) - represents city driving conditions; it is used for light duty vehicle testing"
  extends Base(
    drivingCycleName = "UDDS",
    fileName = Modelica.Utilities.Files.loadResource("modelica://DrivingCycles/Resources/DrivingCycles/UDDS.txt"),
    T = 1369);
    annotation(defaultComponentName = "drivingCycle", defaultComponentPrefixes = "parameter",
    Documentation(info="<html>
<p>Data source <a href=\"https://imee.pl/pub/drive-cycles\">https://imee.pl/pub/drive-cycles</a></p>
</html>"));
end UDDS;
