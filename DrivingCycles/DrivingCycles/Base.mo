within DrivingCycles.DrivingCycles;
record Base "Base driving cycle record"
  extends Modelica.Icons.Record;
  parameter String drivingCycleName = "" "Name of driving cycle";
  parameter String fileName = "" "File name of driving cycle";
  parameter Modelica.Units.SI.Time T(start = 0) "Length of driving cycle";
  annotation(defaultComponentName = "drivingCycle", defaultComponentPrefixes = "parameter",
    Icon(graphics={
        Text(
          textColor={0,0,0},
          extent={{-150,-150},{150,-110}},
          textString="%drivingCycleName")}));
end Base;
