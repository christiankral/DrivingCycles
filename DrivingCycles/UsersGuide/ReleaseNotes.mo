within DrivingCycles.UsersGuide;
class ReleaseNotes "Release Notes"
  extends Modelica.Icons.ReleaseNotes;
  annotation (preferredView="info",Documentation(info="<html>

<h5>Version v0.1.0, 2021-07-18</h5>
<ul>
<li>Initial version</li>
</ul></html>"));
end ReleaseNotes;
