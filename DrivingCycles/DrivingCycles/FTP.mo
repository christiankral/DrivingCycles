within DrivingCycles.DrivingCycles;
record FTP
  "The Federal Test Procedure (FTP) is composed of the UDDS followed by the first 505 seconds of the UDDS"
  extends Base(
    drivingCycleName = "FTP",
    fileName = Modelica.Utilities.Files.loadResource("modelica://DrivingCycles/Resources/DrivingCycles/FTP.txt"),
    T = 1874);
    annotation(defaultComponentName = "drivingCycle", defaultComponentPrefixes = "parameter",
    Documentation(info="<html>
<p>Data source <a href=\"https://imee.pl/pub/drive-cycles\">https://imee.pl/pub/drive-cycles</a></p>
</html>"));
end FTP;
