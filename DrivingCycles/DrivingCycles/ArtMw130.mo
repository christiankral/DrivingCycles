within DrivingCycles.DrivingCycles;
record ArtMw130
  "Driving cycle is based on a statistical study done in Europe; maximum velocity is 130 km/h"
  extends Base(
    drivingCycleName = "ArtMw130",
    fileName = Modelica.Utilities.Files.loadResource("modelica://DrivingCycles/Resources/DrivingCycles/ArtMw130.txt"),
    T = 1068);
    annotation(defaultComponentName = "drivingCycle", defaultComponentPrefixes = "parameter",
    Documentation(info="<html>
<p>Data source <a href=\"https://imee.pl/pub/drive-cycles\">https://imee.pl/pub/drive-cycles</a></p>
</html>"));
end ArtMw130;
