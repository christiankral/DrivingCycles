within DrivingCycles;
package UsersGuide "User's Guide"
  extends Modelica.Icons.Information;

  annotation (preferredView="info",Documentation(info="<html>
<p>
This is the library provides standard driving cycles obtained from the following public sources:
</p>

<ul>
<li><a href=\"https://imee.pl/pub/drive-cycles\">https://imee.pl/pub/drive-cycles</a>
<li><a href=\"https://www.epa.gov/vehicle-and-fuel-emissions-testing/dynamometer-drive-schedules\">https://www.epa.gov/vehicle-and-fuel-emissions-testing/dynamometer-drive-schedules</a>
</ul>
</html>"));
end UsersGuide;
