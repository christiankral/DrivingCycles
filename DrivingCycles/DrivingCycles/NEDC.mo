within DrivingCycles.DrivingCycles;
record NEDC "Artificial driving cycle based on theoretical average utilization of a vehicle
used for homologating vehicles until Euro6 norm in Europe"
  extends Base(
    drivingCycleName = "NEDC",
    fileName = Modelica.Utilities.Files.loadResource("modelica://DrivingCycles/Resources/DrivingCycles/NEDC.txt"),
    T = 1180);
    annotation(defaultComponentName = "drivingCycle", defaultComponentPrefixes = "parameter",
    Documentation(info="<html>
<p>Data source <a href=\"https://imee.pl/pub/drive-cycles\">https://imee.pl/pub/drive-cycles</a></p>
</html>"));
end NEDC;
