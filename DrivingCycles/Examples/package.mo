within DrivingCycles;
package Examples "Driving cycle examples"
extends Modelica.Icons.ExamplesPackage;

  annotation (Documentation(info="<html>
<p>Example(s) of how to use and apply driving cycles.</p>
</html>"));
end Examples;
