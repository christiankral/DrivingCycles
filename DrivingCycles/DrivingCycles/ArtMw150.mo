within DrivingCycles.DrivingCycles;
record ArtMw150
  "Driving cycle is based on a statistical study done in Europe; maximum velocity is 150 km/h"
  extends Base(
    drivingCycleName = "ArtMw150",
    fileName = Modelica.Utilities.Files.loadResource("modelica://DrivingCycles/Resources/DrivingCycles/ArtMw150.txt"),
    T = 1068);
    annotation(defaultComponentName = "drivingCycle", defaultComponentPrefixes = "parameter",
    Documentation(info="<html>
<p>Data source <a href=\"https://imee.pl/pub/drive-cycles\">https://imee.pl/pub/drive-cycles</a></p>
</html>"));
end ArtMw150;
