within DrivingCycles.DrivingCycles;
record WLTP3
  "Worldwide Harmonized Light Vehicle Test Procedure (WLTP) Class 3"
  extends Base(
    drivingCycleName = "WLTP3",
    fileName = Modelica.Utilities.Files.loadResource("modelica://DrivingCycles/Resources/DrivingCycles/WLTP3.txt"),
    T = 1800);
    annotation(defaultComponentName = "drivingCycle", defaultComponentPrefixes = "parameter",
    Documentation(info="<html>
<p>Data source <a href=\"https://imee.pl/pub/drive-cycles\">https://imee.pl/pub/drive-cycles</a></p>
</html>"));
end WLTP3;
