within ;
package DrivingCycles "Driving cycles of vehicles"
extends Modelica.Icons.Package;

  annotation (version="0.X.X", versionDate = "2021-XX-XX",
  uses(Modelica(version="4.0.0")), Documentation(info="<html>
<p>This is the library provides standard driving cycles obtained from public sources</p>
</html>"));
end DrivingCycles;
