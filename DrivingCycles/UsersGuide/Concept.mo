within DrivingCycles.UsersGuide;
class Concept "Fundamental wave concept"
  extends Modelica.Icons.Information;
  annotation (preferredView="info",Documentation(info="<html>
<p>The driving cycle data are stored in external files located in the sub directory 
<code>Resources/DrivingCycles</code>. A use case is shown in example 
<a href=\"modelica://DrivingCycles.Examples.DrivingCycleFromFile\">DrivingCycleFromFile</a>.</p>
</html>"));
end Concept;
