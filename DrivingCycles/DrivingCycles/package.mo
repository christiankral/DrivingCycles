within DrivingCycles;
package DrivingCycles "Driving cycle records"
extends Modelica.Icons.RecordsPackage;

  annotation (Documentation(info="<html>
<p>This package provides a base record definition of a driving cycle and a some selected driving cycle records.</p>
</html>"));
end DrivingCycles;
