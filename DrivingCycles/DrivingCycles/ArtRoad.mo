within DrivingCycles.DrivingCycles;
record ArtRoad
  "Driving cycle is based on a statistical study done in Europe; driving on a rural road"
  extends Base(
    drivingCycleName = "ArtRoad",
    fileName = Modelica.Utilities.Files.loadResource("modelica://DrivingCycles/Resources/DrivingCycles/ArtRoad.txt"),
    T = 1082);
    annotation(defaultComponentName = "drivingCycle", defaultComponentPrefixes = "parameter",
    Documentation(info="<html>
<p>Data source <a href=\"https://imee.pl/pub/drive-cycles\">https://imee.pl/pub/drive-cycles</a></p>
</html>"));
end ArtRoad;
