within DrivingCycles.DrivingCycles;
record WLTP1
  "Worldwide Harmonized Light Vehicle Test Procedure (WLTP) Class 1"
  extends Base(
    drivingCycleName = "WLTP1",
    fileName = Modelica.Utilities.Files.loadResource("modelica://DrivingCycles/Resources/DrivingCycles/WLTP1.txt"),
    T = 1023);
    annotation(defaultComponentName = "drivingCycle", defaultComponentPrefixes = "parameter",
    Documentation(info="<html>
<p>Data source <a href=\"https://imee.pl/pub/drive-cycles\">https://imee.pl/pub/drive-cycles</a></p>
</html>"));
end WLTP1;
